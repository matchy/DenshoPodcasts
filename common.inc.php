<?php
mb_language('Japanese');
mb_internal_encoding('UTF-8');
mb_detect_order("eucJP-win, UTF-8, SJIS-win, jis");
date_default_timezone_set('Asia/Tokyo');

function strFilter($src) {
  $result = trim($src);
  $result = mb_convert_encoding($result, 'UTF-8', 'auto');
  $result = mb_convert_kana($result, 'a', 'UTF-8');
  return $result;
}
/*
 * -*- settings for emacs. -*-
 * Local Variables:
 *   mode:php
 *   indent-tabs-mode: nil
 *   c-basic-offset: 2
 * End:
 */
?>
