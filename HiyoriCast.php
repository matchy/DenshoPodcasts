#!/usr/bin/php
<?php
require_once('simplehtmldom-1.5/simple_html_dom.php');
require_once('common.inc.php');

$INDEXPAGES = array('http://hiyoriweb.net/e-books/e-books2011.html',
		   'http://hiyoriweb.net/e-books/pg81.html',
		   'http://hiyoriweb.net/e-books/e-books2013.html',
		   'http://hiyoriweb.net/e-books/pg102.html',
		   'http://hiyoriweb.net/e-books/pg143.html',
		   'http://hiyoriweb.net/e-books/e-books2016.html');


$buff = array();

foreach ($INDEXPAGES as $indexPage) {

  $dom = file_get_html($indexPage);
  $bmcs = $dom->find('div.bmc');

  foreach ($bmcs as $bmc) {
    $title = $bmc->find('h2 span', 0)->innertext;
    $title = strFilter($title);
    $link = $bmc->find('a', 0)->href;
    $m = array();
    if (preg_match('/\\.\\.(.+$)/', $link, $m)) {
      $link = 'http://hiyoriweb.net' . $m[1];
    }
    $m = array();
    preg_match('/([0-9]+(_.+)?)$/', $title, $m);
    $buff[$m[1]] = array('title' => $title,
			 'uri' => $link);
  }
}
krsort($buff, SORT_NUMERIC);

echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
  <channel>
    <title>長野県のフリーペーパー「日和」PDFCast</title>
<?php
foreach ($buff as $item) {
?>
    <item>
      <title><?php echo $item['title']; ?></title>
      <enclosure url="<?php echo $item['uri']; ?>"
                 type="application/pdf" />
      <guid isPermaLink="true"><?php echo $item['uri']; ?></guid>
    </item>
<?php
}
?>
  </channel>
</rss>
