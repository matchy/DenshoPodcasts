#!/usr/bin/php
<?php
require_once('simplehtmldom-1.5/simple_html_dom.php');
require_once('common.inc.php');

define('BASE_URI', 'http://www.city.chikuma.lg.jp');

$buff = array();

$dom = file_get_html(BASE_URI . '/tag/%E5%B8%82%E5%A0%B1');
$list = $dom->find('li a');

foreach ($list as $key => $val) {
  $year = $val->innertext;
  $m = array();
  if (!preg_match('/([0-9]{4})年/', $year, $m)) {
    continue;
  }
  $year = $m[1];

  $yearuri = BASE_URI . $val->href;
  $dom2 = file_get_html($yearuri);
  $list2 = $dom2->find('h2');
  $list3 = $dom2->find('a.iconPdf');

  foreach ($list2 as $key2 => $val2) {
    $m = array();
    if (!preg_match('/([0-9０-９]+)月号/', $val2->innertext, $m)) {
      continue;
    }
    $month = mb_convert_kana($m[1], 'a', 'UTF-8');
#    if (strlen($month) < 2) {
#      $month = "0{$month}";
#    }
    $month = intval($month);

    $title = preg_replace('/&nbsp;/', '', strip_tags($val2->innertext));

    $uri = '';
    foreach ($list3 as $key3 => $val3) {
      if (preg_match("/{$year}.*{$month}.*(一括|全部|全ページ)/", $val3->innertext)) {
        $uri = $val3->href;
        $m = array();
        if (preg_match('#\\./(.+)#', $uri, $m)) {
          $uri = $m[1];
        }
      }
    }

    $buff[$year][] = array('year' => $year,
                 	   'month'=> $month,
                           'title' => $title,
			   'time' => mktime(0, 0, 0, $month, 1, $year),
                           'uri' => $yearuri . $uri);
  }
}
krsort($buff, SORT_NUMERIC);

echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
  <channel>
    <title>千曲市報</title>
<?php
foreach ($buff as $year => $mval) {
  foreach ($mval as $index => $item) {
?>
    <item>
      <title><?php echo "{$year}年{$item['title']}"; ?></title>
      <enclosure url="<?php echo $item['uri']; ?>"
                 type="application/pdf" />
      <guid isPermaLink="true"><?php echo $item['uri']; ?></guid>
      <pubDate><?php echo date('r', $item['time']); ?></pubDate>
    </item>
<?php
  }
}
?>
  </channel>
</rss>
